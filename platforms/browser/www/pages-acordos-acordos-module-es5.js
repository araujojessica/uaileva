(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-acordos-acordos-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/acordos/acordos.page.html":
    /*!***************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/acordos/acordos.page.html ***!
      \***************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesAcordosAcordosPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button text=\"\" color=\"light\"></ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <h1>Consentimento legal</h1>\n\n  <div>\n    <p class=\"declaracao\">Declaração de checagem de antecedentes</p>\n    <p class=\"termo\">\n      Lorem Ipsum is simply dummy text of the printing and typesetting industry. \n      Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, \n      when an unknown printer took a galley of type and scrambled it to make a type \n      specimen book. It has survived not only five centuries, but also the leap into \n      electronic typesetting, remaining essentially unchanged. It was popularised in \n      the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, \n      and more recently with desktop publishing software like Aldus PageMaker including \n      versions of Lorem Ipsum.</p>\n  </div>\n\n  <ion-item>\n    <ion-checkbox item-start (ionChange)=\"enableBtn($event)\"></ion-checkbox>&nbsp;\n    <ion-label>Estou ciente e aceito</ion-label>\n  </ion-item><br>\n\n\n  <ion-button [disabled]=\"checkedButton\" class=\"botao\">Aceito</ion-button>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/pages/acordos/acordos-routing.module.ts":
    /*!*********************************************************!*\
      !*** ./src/app/pages/acordos/acordos-routing.module.ts ***!
      \*********************************************************/

    /*! exports provided: AcordosPageRoutingModule */

    /***/
    function srcAppPagesAcordosAcordosRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AcordosPageRoutingModule", function () {
        return AcordosPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _acordos_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./acordos.page */
      "./src/app/pages/acordos/acordos.page.ts");

      var routes = [{
        path: '',
        component: _acordos_page__WEBPACK_IMPORTED_MODULE_3__["AcordosPage"]
      }];

      var AcordosPageRoutingModule = function AcordosPageRoutingModule() {
        _classCallCheck(this, AcordosPageRoutingModule);
      };

      AcordosPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], AcordosPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/acordos/acordos.module.ts":
    /*!*************************************************!*\
      !*** ./src/app/pages/acordos/acordos.module.ts ***!
      \*************************************************/

    /*! exports provided: AcordosPageModule */

    /***/
    function srcAppPagesAcordosAcordosModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AcordosPageModule", function () {
        return AcordosPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _acordos_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./acordos-routing.module */
      "./src/app/pages/acordos/acordos-routing.module.ts");
      /* harmony import */


      var _acordos_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./acordos.page */
      "./src/app/pages/acordos/acordos.page.ts");

      var AcordosPageModule = function AcordosPageModule() {
        _classCallCheck(this, AcordosPageModule);
      };

      AcordosPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _acordos_routing_module__WEBPACK_IMPORTED_MODULE_5__["AcordosPageRoutingModule"]],
        declarations: [_acordos_page__WEBPACK_IMPORTED_MODULE_6__["AcordosPage"]]
      })], AcordosPageModule);
      /***/
    },

    /***/
    "./src/app/pages/acordos/acordos.page.scss":
    /*!*************************************************!*\
      !*** ./src/app/pages/acordos/acordos.page.scss ***!
      \*************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesAcordosAcordosPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content {\n  --background: var(--ion-color-light);\n  --background: url(\"/src/assets/img/bg-uai-leva2.png\") #eaeaea no-repeat bottom 75px left 45px;\n}\n\nion-toolbar {\n  --background: #001c4e !important;\n}\n\nion-header {\n  --background: #001c4e !important;\n}\n\nion-item {\n  --ion-item-background: transparent;\n  --border-style: var(--border-style);\n  --border: 0 none;\n  --box-shadow: 0 0 0 0;\n  --outline: 0;\n}\n\nion-icon {\n  color: gray;\n}\n\nh1 {\n  font-size: 1.5rem;\n  text-align: center;\n  margin: 15px 0;\n  font-weight: bold;\n  color: #123b7d;\n}\n\n.declaracao {\n  font-size: 1.1rem;\n  padding: 10px;\n  color: #123b7d;\n}\n\n.termo {\n  padding: 10px;\n  padding-top: 0px;\n  color: black;\n  margin-top: -20px;\n  text-align: justify;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYWNvcmRvcy9hY29yZG9zLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG9DQUFBO0VBQ0EsNkZBQUE7QUFDSjs7QUFFQTtFQUNJLGdDQUFBO0FBQ0o7O0FBRUE7RUFDSSxnQ0FBQTtBQUNKOztBQUVBO0VBQ0ksa0NBQUE7RUFDQSxtQ0FBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0FBQ0o7O0FBRUE7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0VBRUEsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQUFKOztBQUdBO0VBQ0ksaUJBQUE7RUFFQSxhQUFBO0VBRUEsY0FBQTtBQUZKOztBQUtBO0VBQ0ksYUFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUFGSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2Fjb3Jkb3MvYWNvcmRvcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XHJcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XHJcbiAgICAtLWJhY2tncm91bmQ6IHVybChcIi9zcmMvYXNzZXRzL2ltZy9iZy11YWktbGV2YTIucG5nXCIpICNlYWVhZWEgbm8tcmVwZWF0IGJvdHRvbSA3NXB4IGxlZnQgNDVweDtcclxufVxyXG5cclxuaW9uLXRvb2xiYXIge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjMDAxYzRlICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbmlvbi1oZWFkZXIge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjMDAxYzRlICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbmlvbi1pdGVtIHtcclxuICAgIC0taW9uLWl0ZW0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICAtLWJvcmRlci1zdHlsZTogdmFyKC0tYm9yZGVyLXN0eWxlKTtcclxuICAgIC0tYm9yZGVyOiAwIG5vbmU7IFxyXG4gICAgLS1ib3gtc2hhZG93OiAwIDAgMCAwOyBcclxuICAgIC0tb3V0bGluZTogMDtcclxufVxyXG5cclxuaW9uLWljb24ge1xyXG4gICAgY29sb3I6IGdyYXk7XHJcbn1cclxuXHJcbmgxIHtcclxuICAgIGZvbnQtc2l6ZTogMS41cmVtO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgLy90ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgbWFyZ2luOiAxNXB4IDA7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGNvbG9yOiAjMTIzYjdkO1xyXG59XHJcblxyXG4uZGVjbGFyYWNhbyB7XHJcbiAgICBmb250LXNpemU6IDEuMXJlbTtcclxuICAgIC8vZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgLy9wYWRkaW5nLWJvdHRvbTogMHB4O1xyXG4gICAgY29sb3I6ICMxMjNiN2Q7XHJcbn1cclxuXHJcbi50ZXJtbyB7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgcGFkZGluZy10b3A6IDBweDtcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIG1hcmdpbi10b3A6IC0yMHB4O1xyXG4gICAgdGV4dC1hbGlnbjoganVzdGlmeTtcclxufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/pages/acordos/acordos.page.ts":
    /*!***********************************************!*\
      !*** ./src/app/pages/acordos/acordos.page.ts ***!
      \***********************************************/

    /*! exports provided: AcordosPage */

    /***/
    function srcAppPagesAcordosAcordosPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AcordosPage", function () {
        return AcordosPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var AcordosPage = /*#__PURE__*/function () {
        function AcordosPage() {
          _classCallCheck(this, AcordosPage);

          this.checkedButton = true;
        }

        _createClass(AcordosPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "enableBtn",
          value: function enableBtn(event) {
            if (event.checked) {
              this.checkedButton = !this.checkedButton;
            } else {
              this.checkedButton = !this.checkedButton;
            }
          }
        }]);

        return AcordosPage;
      }();

      AcordosPage.ctorParameters = function () {
        return [];
      };

      AcordosPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-acordos',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./acordos.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/acordos/acordos.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./acordos.page.scss */
        "./src/app/pages/acordos/acordos.page.scss"))["default"]]
      })], AcordosPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-acordos-acordos-module-es5.js.map