(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-etapas-etapas-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/etapas/etapas.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/etapas/etapas.page.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button text=\"\" color=\"light\"></ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <h1>Etapas Obrigatórias</h1>\n  <div>\n    <p class=\"usuario\">Olá, Jéssica</p>\n    <p class=\"comecar\">Veja aqui o que você precisa fazer para criar sua conta.</p>\n  </div>\n\n  <ion-list>\n    <ion-item no-padding (click)=\"toAcordos()\">\n      <ion-icon name=\"reader-outline\" slot=\"start\"></ion-icon>\n      <ion-label>\n        Acordos Legais<br>\n        <p>Próxima etapa recomendada</p>\n      </ion-label>\n      <ion-icon name=\"chevron-forward-outline\" slot=\"end\" size=\"small\" style=\"color: #001c4e;\"></ion-icon>\n    </ion-item>\n\n    <ion-item no-padding (click)=\"toFotoPerfil()\">\n      <ion-icon name=\"reader-outline\" slot=\"start\"></ion-icon>\n      <ion-label>\n        Foto de Perfil<br>\n        <p>Tudo pronto para começar</p>\n      </ion-label>\n      <ion-icon name=\"chevron-forward-outline\" slot=\"end\" size=\"small\" style=\"color: #001c4e;\"></ion-icon>\n    </ion-item>\n\n    <ion-item no-padding (click)=\"toCNH()\">\n      <ion-icon name=\"reader-outline\" slot=\"start\"></ion-icon>\n      <ion-label>\n        Carteira Nacional de Habilitação <br> com EAR - CNH <br>\n        <p>Tudo pronto para começar</p>\n      </ion-label>\n      <ion-icon name=\"chevron-forward-outline\" slot=\"end\" size=\"small\" style=\"color: #001c4e;\"></ion-icon>\n    </ion-item>\n\n    <ion-item no-padding (click)=\"toCRLV()\">\n      <ion-icon name=\"reader-outline\" slot=\"start\"></ion-icon>\n      <ion-label>\n        Certificado de Registro e<br>Licenciamento de Veículo - CRLV<br>\n        <p>Tudo pronto para começar</p>\n      </ion-label>\n      <ion-icon name=\"chevron-forward-outline\" slot=\"end\" size=\"small\" style=\"color: #001c4e;\"></ion-icon>\n    </ion-item>\n  </ion-list>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/etapas/etapas-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/etapas/etapas-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: EtapasPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EtapasPageRoutingModule", function() { return EtapasPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _etapas_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./etapas.page */ "./src/app/pages/etapas/etapas.page.ts");




const routes = [
    {
        path: '',
        component: _etapas_page__WEBPACK_IMPORTED_MODULE_3__["EtapasPage"]
    }
];
let EtapasPageRoutingModule = class EtapasPageRoutingModule {
};
EtapasPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], EtapasPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/etapas/etapas.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/etapas/etapas.module.ts ***!
  \***********************************************/
/*! exports provided: EtapasPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EtapasPageModule", function() { return EtapasPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _etapas_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./etapas-routing.module */ "./src/app/pages/etapas/etapas-routing.module.ts");
/* harmony import */ var _etapas_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./etapas.page */ "./src/app/pages/etapas/etapas.page.ts");







let EtapasPageModule = class EtapasPageModule {
};
EtapasPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _etapas_routing_module__WEBPACK_IMPORTED_MODULE_5__["EtapasPageRoutingModule"]
        ],
        declarations: [_etapas_page__WEBPACK_IMPORTED_MODULE_6__["EtapasPage"]]
    })
], EtapasPageModule);



/***/ }),

/***/ "./src/app/pages/etapas/etapas.page.scss":
/*!***********************************************!*\
  !*** ./src/app/pages/etapas/etapas.page.scss ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --background: var(--ion-color-light);\n  --background: url(\"/src/assets/img/bg-uai-leva2.png\") #eaeaea no-repeat bottom 75px left 45px;\n}\n\nion-toolbar {\n  --background: #001c4e !important;\n}\n\nion-header {\n  --background: #001c4e !important;\n}\n\nion-list {\n  background: transparent;\n  padding: 10px;\n}\n\nion-label {\n  color: #001c4e;\n}\n\nh1 {\n  font-size: 1.5rem;\n  text-align: center;\n  text-transform: uppercase;\n  margin: 15px 0;\n  font-weight: bold;\n  color: #123b7d;\n}\n\n.usuario {\n  font-size: 1.4rem;\n  font-weight: bold;\n  padding: 10px;\n  padding-bottom: 0px;\n  color: #123b7d;\n}\n\n.comecar {\n  padding: 10px;\n  padding-top: 0px;\n  color: #123b7d;\n  margin-top: -20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZXRhcGFzL2V0YXBhcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxvQ0FBQTtFQUNBLDZGQUFBO0FBQ0o7O0FBRUE7RUFDSSxnQ0FBQTtBQUNKOztBQUVBO0VBQ0ksZ0NBQUE7QUFDSjs7QUFFQTtFQUNJLHVCQUFBO0VBQ0EsYUFBQTtBQUNKOztBQUVBO0VBQ0ksY0FBQTtBQUNKOztBQUVBO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQUNKOztBQUVBO0VBQ0ksaUJBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7QUFDSjs7QUFFQTtFQUNJLGFBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtBQUNKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZXRhcGFzL2V0YXBhcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XHJcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XHJcbiAgICAtLWJhY2tncm91bmQ6IHVybChcIi9zcmMvYXNzZXRzL2ltZy9iZy11YWktbGV2YTIucG5nXCIpICNlYWVhZWEgbm8tcmVwZWF0IGJvdHRvbSA3NXB4IGxlZnQgNDVweDtcclxufVxyXG5cclxuaW9uLXRvb2xiYXIge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjMDAxYzRlICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbmlvbi1oZWFkZXIge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjMDAxYzRlICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbmlvbi1saXN0IHtcclxuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgcGFkZGluZzogMTBweDtcclxufVxyXG5cclxuaW9uLWxhYmVsIHtcclxuICAgIGNvbG9yOiAjMDAxYzRlO1xyXG59XHJcblxyXG5oMSB7XHJcbiAgICBmb250LXNpemU6IDEuNXJlbTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICBtYXJnaW46IDE1cHggMDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgY29sb3I6ICMxMjNiN2Q7XHJcbn1cclxuXHJcbi51c3VhcmlvIHtcclxuICAgIGZvbnQtc2l6ZTogMS40cmVtO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgcGFkZGluZy1ib3R0b206IDBweDtcclxuICAgIGNvbG9yOiAjMTIzYjdkO1xyXG59XHJcblxyXG4uY29tZWNhciB7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgcGFkZGluZy10b3A6IDBweDtcclxuICAgIGNvbG9yOiAjMTIzYjdkO1xyXG4gICAgbWFyZ2luLXRvcDogLTIwcHg7XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/etapas/etapas.page.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/etapas/etapas.page.ts ***!
  \*********************************************/
/*! exports provided: EtapasPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EtapasPage", function() { return EtapasPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");



let EtapasPage = class EtapasPage {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() {
    }
    toAcordos() {
        this.router.navigate(['/acordos']);
    }
    toFotoPerfil() {
        this.router.navigate(['/foto-perfil']);
    }
    toCNH() {
        this.router.navigate(['/cnh']);
    }
    toCRLV() {
        this.router.navigate(['/crlv']);
    }
};
EtapasPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
EtapasPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-etapas',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./etapas.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/etapas/etapas.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./etapas.page.scss */ "./src/app/pages/etapas/etapas.page.scss")).default]
    })
], EtapasPage);



/***/ })

}]);
//# sourceMappingURL=pages-etapas-etapas-module-es2015.js.map