(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-crlv-crlv-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/crlv/crlv.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/crlv/crlv.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button text=\"\" color=\"light\"></ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <h1>Tire uma foto do(a) Ceetificado de Registro e Licenciamento de Veículo - CRLV</h1>\n  <br>\n  <div>\n    <p class=\"termo\">\n      Lembre-se, é preciso que: 1. A foto mostre todo o documento, como na imagem abaixo. \n      2. Todos os campos estejam legíveis. 3. A categoria do veículo apareça como \"Particular\".\n      4. O veículo não seja anterior a 2010. Obs: O CRLV eletrônico é aceito regularmente.\n    </p>\n  </div>\n\n  <img src=\"assets/img/exemplo-crlv.jpg\"><br>\n\n  <ion-button class=\"botao\">Enviar foto</ion-button>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/crlv/crlv-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/crlv/crlv-routing.module.ts ***!
  \***************************************************/
/*! exports provided: CRLVPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CRLVPageRoutingModule", function() { return CRLVPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _crlv_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./crlv.page */ "./src/app/pages/crlv/crlv.page.ts");




const routes = [
    {
        path: '',
        component: _crlv_page__WEBPACK_IMPORTED_MODULE_3__["CRLVPage"]
    }
];
let CRLVPageRoutingModule = class CRLVPageRoutingModule {
};
CRLVPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CRLVPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/crlv/crlv.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/crlv/crlv.module.ts ***!
  \*******************************************/
/*! exports provided: CRLVPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CRLVPageModule", function() { return CRLVPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _crlv_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./crlv-routing.module */ "./src/app/pages/crlv/crlv-routing.module.ts");
/* harmony import */ var _crlv_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./crlv.page */ "./src/app/pages/crlv/crlv.page.ts");







let CRLVPageModule = class CRLVPageModule {
};
CRLVPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _crlv_routing_module__WEBPACK_IMPORTED_MODULE_5__["CRLVPageRoutingModule"]
        ],
        declarations: [_crlv_page__WEBPACK_IMPORTED_MODULE_6__["CRLVPage"]]
    })
], CRLVPageModule);



/***/ }),

/***/ "./src/app/pages/crlv/crlv.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/crlv/crlv.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --background: var(--ion-color-light);\n  --background: url(\"/src/assets/img/bg-uai-leva2.png\") #eaeaea no-repeat bottom 75px left 45px;\n}\n\nion-toolbar {\n  --background: #001c4e !important;\n}\n\nion-header {\n  --background: #001c4e !important;\n}\n\nion-item {\n  --ion-item-background: transparent;\n  --border-style: var(--border-style);\n  --border: 0 none;\n  --box-shadow: 0 0 0 0;\n  --outline: 0;\n}\n\nion-icon {\n  color: gray;\n}\n\nimg {\n  height: 300px;\n  width: 300px;\n  display: block;\n  margin: auto;\n}\n\nh1 {\n  font-size: 1.5rem;\n  text-align: center;\n  margin: 15px 0;\n  font-weight: bold;\n  color: #123b7d;\n}\n\n.declaracao {\n  font-size: 1.1rem;\n  padding: 10px;\n  color: #123b7d;\n  text-align: justify;\n}\n\n.termo {\n  padding: 10px;\n  padding-top: 0px;\n  color: #123b7d;\n  margin-top: -20px;\n  text-align: justify;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY3Jsdi9jcmx2LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG9DQUFBO0VBQ0EsNkZBQUE7QUFDSjs7QUFFQTtFQUNJLGdDQUFBO0FBQ0o7O0FBRUE7RUFDSSxnQ0FBQTtBQUNKOztBQUVBO0VBQ0ksa0NBQUE7RUFDQSxtQ0FBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0FBQ0o7O0FBRUE7RUFDSSxhQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0VBRUEsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQUFKOztBQUdBO0VBQ0ksaUJBQUE7RUFFQSxhQUFBO0VBRUEsY0FBQTtFQUNBLG1CQUFBO0FBRko7O0FBS0E7RUFDSSxhQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtBQUZKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvY3Jsdi9jcmx2LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcclxuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLWxpZ2h0KTtcclxuICAgIC0tYmFja2dyb3VuZDogdXJsKFwiL3NyYy9hc3NldHMvaW1nL2JnLXVhaS1sZXZhMi5wbmdcIikgI2VhZWFlYSBuby1yZXBlYXQgYm90dG9tIDc1cHggbGVmdCA0NXB4O1xyXG59XHJcblxyXG5pb24tdG9vbGJhciB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICMwMDFjNGUgIWltcG9ydGFudDtcclxufVxyXG5cclxuaW9uLWhlYWRlciB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICMwMDFjNGUgIWltcG9ydGFudDtcclxufVxyXG5cclxuaW9uLWl0ZW0ge1xyXG4gICAgLS1pb24taXRlbS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgIC0tYm9yZGVyLXN0eWxlOiB2YXIoLS1ib3JkZXItc3R5bGUpO1xyXG4gICAgLS1ib3JkZXI6IDAgbm9uZTsgXHJcbiAgICAtLWJveC1zaGFkb3c6IDAgMCAwIDA7IFxyXG4gICAgLS1vdXRsaW5lOiAwO1xyXG59XHJcblxyXG5pb24taWNvbiB7XHJcbiAgICBjb2xvcjogZ3JheTtcclxufVxyXG5cclxuaW1nIHtcclxuICAgIGhlaWdodDogMzAwcHg7XHJcbiAgICB3aWR0aDogMzAwcHg7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIG1hcmdpbjogYXV0bztcclxufVxyXG5cclxuaDEge1xyXG4gICAgZm9udC1zaXplOiAxLjVyZW07XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAvL3RleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICBtYXJnaW46IDE1cHggMDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgY29sb3I6ICMxMjNiN2Q7XHJcbn1cclxuXHJcbi5kZWNsYXJhY2FvIHtcclxuICAgIGZvbnQtc2l6ZTogMS4xcmVtO1xyXG4gICAgLy9mb250LXdlaWdodDogYm9sZDtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAvL3BhZGRpbmctYm90dG9tOiAwcHg7XHJcbiAgICBjb2xvcjogIzEyM2I3ZDtcclxuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XHJcbn1cclxuXHJcbi50ZXJtbyB7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgcGFkZGluZy10b3A6IDBweDtcclxuICAgIGNvbG9yOiAjMTIzYjdkO1xyXG4gICAgbWFyZ2luLXRvcDogLTIwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xyXG59Il19 */");

/***/ }),

/***/ "./src/app/pages/crlv/crlv.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/crlv/crlv.page.ts ***!
  \*****************************************/
/*! exports provided: CRLVPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CRLVPage", function() { return CRLVPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let CRLVPage = class CRLVPage {
    constructor() { }
    ngOnInit() {
    }
};
CRLVPage.ctorParameters = () => [];
CRLVPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-crlv',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./crlv.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/crlv/crlv.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./crlv.page.scss */ "./src/app/pages/crlv/crlv.page.scss")).default]
    })
], CRLVPage);



/***/ })

}]);
//# sourceMappingURL=pages-crlv-crlv-module-es2015.js.map