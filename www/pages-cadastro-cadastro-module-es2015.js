(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-cadastro-cadastro-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/cadastro/cadastro.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/cadastro/cadastro.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button text=\"\" color=\"light\"></ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding >\n  <h1>Cadastro</h1>\n  <form style=\"padding-left: 10px; padding-right: 10px;\"> \n    <ion-item>\n      <ion-input type=\"text\" name=\"nome\" placeholder=\"Nome\">\n        <ion-icon name=\"person-outline\" size=\"small\" slot=\"start\" item-start></ion-icon>&nbsp;\n      </ion-input>\n    </ion-item>\n\n    <p></p>\n\n    <ion-item>\n      <ion-input type=\"text\" name=\"email\" placeholder=\"E-mail\">\n        <ion-icon name=\"mail-outline\" size=\"small\" slot=\"start\"></ion-icon>&nbsp;\n      </ion-input>\n    </ion-item>\n\n    <p></p>\n\n    <ion-item>\n      <ion-input [type]=\"tipo ? 'text' : 'password'\" name=\"senha\" placeholder=\"Senha\">\n        <ion-icon name=\"lock-closed-outline\" size=\"small\" slot=\"start\"></ion-icon>&nbsp;\n      </ion-input>\n      <ion-icon [name]=\"tipo ? 'eye-outline' : 'eye-off-outline'\" slot=\"end\" size=\"small\" (click)=\"mostrarSenha()\" style=\"padding-top: 5px;\"></ion-icon>\n    </ion-item>\n\n    <p></p>\n    \n    <ion-item> \n      <ion-input type=\"tel\" name=\"telefone\" placeholder=\"Telefone\">\n        <ion-icon name=\"phone-portrait-outline\" size=\"small\" slot=\"start\"></ion-icon>&nbsp;\n      </ion-input>\n    </ion-item>\n  </form>\n  <p></p>\n  <ion-button class=\"botaoCadastro\" (click)=\"toEtapas()\">Cadastrar</ion-button>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/cadastro/cadastro-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/cadastro/cadastro-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: CadastroPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CadastroPageRoutingModule", function() { return CadastroPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _cadastro_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./cadastro.page */ "./src/app/pages/cadastro/cadastro.page.ts");




const routes = [
    {
        path: '',
        component: _cadastro_page__WEBPACK_IMPORTED_MODULE_3__["CadastroPage"]
    }
];
let CadastroPageRoutingModule = class CadastroPageRoutingModule {
};
CadastroPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CadastroPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/cadastro/cadastro.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/cadastro/cadastro.module.ts ***!
  \***************************************************/
/*! exports provided: CadastroPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CadastroPageModule", function() { return CadastroPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _cadastro_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./cadastro-routing.module */ "./src/app/pages/cadastro/cadastro-routing.module.ts");
/* harmony import */ var _cadastro_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./cadastro.page */ "./src/app/pages/cadastro/cadastro.page.ts");







let CadastroPageModule = class CadastroPageModule {
};
CadastroPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _cadastro_routing_module__WEBPACK_IMPORTED_MODULE_5__["CadastroPageRoutingModule"]
        ],
        declarations: [_cadastro_page__WEBPACK_IMPORTED_MODULE_6__["CadastroPage"]]
    })
], CadastroPageModule);



/***/ }),

/***/ "./src/app/pages/cadastro/cadastro.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/cadastro/cadastro.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --background: var(--ion-color-light);\n  --background: url(\"/src/assets/img/bg-uai-leva2.png\") #eaeaea no-repeat bottom 75px left 45px;\n}\n\nion-toolbar {\n  --background: #001c4e !important;\n}\n\nion-header {\n  --background: #001c4e !important;\n}\n\nion-item {\n  border: 1px solid lightgray;\n  border-radius: 50px;\n  --highlight-height: 0px;\n  /*--highlight-color-focused: var(--ion-color-primary, #001c4e);\n  --highlight-color-valid: var(--ion-color-success, #2dd36f);\n  --highlight-color-invalid: var(--ion-color-danger, #eb445a);*/\n}\n\nion-icon {\n  color: gray;\n}\n\nh1 {\n  font-size: 1.5rem;\n  text-align: center;\n  text-transform: uppercase;\n  margin: 15px 0;\n  font-weight: bold;\n  color: #123b7d;\n}\n\n.botaoCadastro {\n  --background: #b1ff49;\n  --color: #001c4e;\n  font-weight: bold;\n  display: block;\n  height: 48px;\n  width: 95%;\n  margin: auto;\n  --border-radius: 50px;\n  --background-activated: #001c4e;\n  --color-activated: white;\n  --background-hover: #001c4e;\n  --color-hover: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY2FkYXN0cm8vY2FkYXN0cm8ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksb0NBQUE7RUFDQSw2RkFBQTtBQUNKOztBQUVBO0VBQ0ksZ0NBQUE7QUFDSjs7QUFFQTtFQUNJLGdDQUFBO0FBQ0o7O0FBRUE7RUFDSSwyQkFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQTs7K0RBQUE7QUFHSjs7QUFFQTtFQUNJLFdBQUE7QUFDSjs7QUFFQTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7QUFDSjs7QUFFQTtFQUNJLHFCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSxxQkFBQTtFQUNBLCtCQUFBO0VBQ0Esd0JBQUE7RUFDQSwyQkFBQTtFQUNBLG9CQUFBO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9jYWRhc3Ryby9jYWRhc3Ryby5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XHJcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XHJcbiAgICAtLWJhY2tncm91bmQ6IHVybChcIi9zcmMvYXNzZXRzL2ltZy9iZy11YWktbGV2YTIucG5nXCIpICNlYWVhZWEgbm8tcmVwZWF0IGJvdHRvbSA3NXB4IGxlZnQgNDVweDtcclxufVxyXG5cclxuaW9uLXRvb2xiYXIge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjMDAxYzRlICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbmlvbi1oZWFkZXIge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjMDAxYzRlICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbmlvbi1pdGVtIHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgICAtLWhpZ2hsaWdodC1oZWlnaHQ6IDBweDtcclxuICAgIC8qLS1oaWdobGlnaHQtY29sb3ItZm9jdXNlZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnksICMwMDFjNGUpO1xyXG4gICAgLS1oaWdobGlnaHQtY29sb3ItdmFsaWQ6IHZhcigtLWlvbi1jb2xvci1zdWNjZXNzLCAjMmRkMzZmKTtcclxuICAgIC0taGlnaGxpZ2h0LWNvbG9yLWludmFsaWQ6IHZhcigtLWlvbi1jb2xvci1kYW5nZXIsICNlYjQ0NWEpOyovXHJcbn1cclxuXHJcbmlvbi1pY29uIHtcclxuICAgIGNvbG9yOiBncmF5O1xyXG59XHJcblxyXG5oMSB7XHJcbiAgICBmb250LXNpemU6IDEuNXJlbTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICBtYXJnaW46IDE1cHggMDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgY29sb3I6ICMxMjNiN2Q7XHJcbn1cclxuXHJcbi5ib3Rhb0NhZGFzdHJvIHtcclxuICAgIC0tYmFja2dyb3VuZDogI2IxZmY0OTsgIC8vIzAwMWM0ZTtcclxuICAgIC0tY29sb3I6ICMwMDFjNGU7IC8vd2hpdGU7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgaGVpZ2h0OiA0OHB4O1xyXG4gICAgd2lkdGg6IDk1JTtcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIC0tYm9yZGVyLXJhZGl1czogNTBweDtcclxuICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6ICMwMDFjNGU7XHJcbiAgICAtLWNvbG9yLWFjdGl2YXRlZDogd2hpdGU7XHJcbiAgICAtLWJhY2tncm91bmQtaG92ZXI6ICMwMDFjNGU7XHJcbiAgICAtLWNvbG9yLWhvdmVyOiB3aGl0ZTtcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/cadastro/cadastro.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/cadastro/cadastro.page.ts ***!
  \*************************************************/
/*! exports provided: CadastroPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CadastroPage", function() { return CadastroPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");



let CadastroPage = class CadastroPage {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() {
    }
    mostrarSenha() {
        this.tipo = !this.tipo;
    }
    toEtapas() {
        this.router.navigate(['/etapas']);
    }
};
CadastroPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
CadastroPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-cadastro',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./cadastro.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/cadastro/cadastro.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./cadastro.page.scss */ "./src/app/pages/cadastro/cadastro.page.scss")).default]
    })
], CadastroPage);



/***/ })

}]);
//# sourceMappingURL=pages-cadastro-cadastro-module-es2015.js.map