(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-avaliacoes-avaliacoes-module"], {
    /***/
    "./src/app/pages/avaliacoes/avaliacoes-routing.module.ts":
    /*!***************************************************************!*\
      !*** ./src/app/pages/avaliacoes/avaliacoes-routing.module.ts ***!
      \***************************************************************/

    /*! exports provided: AvaliacoesPageRoutingModule */

    /***/
    function srcAppPagesAvaliacoesAvaliacoesRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AvaliacoesPageRoutingModule", function () {
        return AvaliacoesPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _avaliacoes_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./avaliacoes.page */
      "./src/app/pages/avaliacoes/avaliacoes.page.ts");

      var routes = [{
        path: '',
        component: _avaliacoes_page__WEBPACK_IMPORTED_MODULE_3__["AvaliacoesPage"]
      }];

      var AvaliacoesPageRoutingModule = function AvaliacoesPageRoutingModule() {
        _classCallCheck(this, AvaliacoesPageRoutingModule);
      };

      AvaliacoesPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], AvaliacoesPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/avaliacoes/avaliacoes.module.ts":
    /*!*******************************************************!*\
      !*** ./src/app/pages/avaliacoes/avaliacoes.module.ts ***!
      \*******************************************************/

    /*! exports provided: AvaliacoesPageModule */

    /***/
    function srcAppPagesAvaliacoesAvaliacoesModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AvaliacoesPageModule", function () {
        return AvaliacoesPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _avaliacoes_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./avaliacoes-routing.module */
      "./src/app/pages/avaliacoes/avaliacoes-routing.module.ts");
      /* harmony import */


      var _avaliacoes_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./avaliacoes.page */
      "./src/app/pages/avaliacoes/avaliacoes.page.ts");

      var AvaliacoesPageModule = function AvaliacoesPageModule() {
        _classCallCheck(this, AvaliacoesPageModule);
      };

      AvaliacoesPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _avaliacoes_routing_module__WEBPACK_IMPORTED_MODULE_5__["AvaliacoesPageRoutingModule"]],
        declarations: [_avaliacoes_page__WEBPACK_IMPORTED_MODULE_6__["AvaliacoesPage"]],
        exports: [_avaliacoes_page__WEBPACK_IMPORTED_MODULE_6__["AvaliacoesPage"]]
      })], AvaliacoesPageModule);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-avaliacoes-avaliacoes-module-es5.js.map