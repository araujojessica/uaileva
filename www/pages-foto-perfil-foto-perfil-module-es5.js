(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-foto-perfil-foto-perfil-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/foto-perfil/foto-perfil.page.html":
    /*!***********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/foto-perfil/foto-perfil.page.html ***!
      \***********************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesFotoPerfilFotoPerfilPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button text=\"\" color=\"light\"></ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <h1>Tire sua foto de perfil</h1>\n\n  <div>\n    <p class=\"declaracao\">\n      Observação: não é possível alterar a foto de perfil depois de enviá-la.\n      Antes de enviar a foto, verifique se te representa com precisão.\n    </p>\n\n    <p class=\"termo\">\n      1. Mostre o rosto todo e a parte de cima dos ombros.<br>\n      2. Não use óculos escuros nem chapéu.<br>\n      3. Tire a foto em um local bem iluminado.<br>\n    </p>\n\n  </div>\n\n  <img src=\"assets/img/teste-avatar.png\"><br>\n\n  <ion-button class=\"botao\">Enviar foto</ion-button>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/foto-perfil/foto-perfil-routing.module.ts":
    /*!*****************************************************************!*\
      !*** ./src/app/pages/foto-perfil/foto-perfil-routing.module.ts ***!
      \*****************************************************************/

    /*! exports provided: FotoPerfilPageRoutingModule */

    /***/
    function srcAppPagesFotoPerfilFotoPerfilRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FotoPerfilPageRoutingModule", function () {
        return FotoPerfilPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _foto_perfil_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./foto-perfil.page */
      "./src/app/pages/foto-perfil/foto-perfil.page.ts");

      var routes = [{
        path: '',
        component: _foto_perfil_page__WEBPACK_IMPORTED_MODULE_3__["FotoPerfilPage"]
      }];

      var FotoPerfilPageRoutingModule = function FotoPerfilPageRoutingModule() {
        _classCallCheck(this, FotoPerfilPageRoutingModule);
      };

      FotoPerfilPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], FotoPerfilPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/foto-perfil/foto-perfil.module.ts":
    /*!*********************************************************!*\
      !*** ./src/app/pages/foto-perfil/foto-perfil.module.ts ***!
      \*********************************************************/

    /*! exports provided: FotoPerfilPageModule */

    /***/
    function srcAppPagesFotoPerfilFotoPerfilModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FotoPerfilPageModule", function () {
        return FotoPerfilPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _foto_perfil_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./foto-perfil-routing.module */
      "./src/app/pages/foto-perfil/foto-perfil-routing.module.ts");
      /* harmony import */


      var _foto_perfil_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./foto-perfil.page */
      "./src/app/pages/foto-perfil/foto-perfil.page.ts");

      var FotoPerfilPageModule = function FotoPerfilPageModule() {
        _classCallCheck(this, FotoPerfilPageModule);
      };

      FotoPerfilPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _foto_perfil_routing_module__WEBPACK_IMPORTED_MODULE_5__["FotoPerfilPageRoutingModule"]],
        declarations: [_foto_perfil_page__WEBPACK_IMPORTED_MODULE_6__["FotoPerfilPage"]]
      })], FotoPerfilPageModule);
      /***/
    },

    /***/
    "./src/app/pages/foto-perfil/foto-perfil.page.scss":
    /*!*********************************************************!*\
      !*** ./src/app/pages/foto-perfil/foto-perfil.page.scss ***!
      \*********************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesFotoPerfilFotoPerfilPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content {\n  --background: var(--ion-color-light);\n  --background: url(\"/src/assets/img/bg-uai-leva2.png\") #eaeaea no-repeat bottom 75px left 45px;\n}\n\nion-toolbar {\n  --background: #001c4e !important;\n}\n\nion-header {\n  --background: #001c4e !important;\n}\n\nion-item {\n  --ion-item-background: transparent;\n  --border-style: var(--border-style);\n  --border: 0 none;\n  --box-shadow: 0 0 0 0;\n  --outline: 0;\n}\n\nion-icon {\n  color: gray;\n}\n\nimg {\n  height: 200px;\n  width: 200px;\n  display: block;\n  margin: auto;\n}\n\nh1 {\n  font-size: 1.5rem;\n  text-align: center;\n  margin: 15px 0;\n  font-weight: bold;\n  color: #123b7d;\n}\n\n.declaracao {\n  font-size: 1.1rem;\n  padding: 10px;\n  color: #123b7d;\n  text-align: justify;\n}\n\n.termo {\n  padding: 10px;\n  padding-top: 0px;\n  color: black;\n  margin-top: -20px;\n  text-align: justify;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZm90by1wZXJmaWwvZm90by1wZXJmaWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksb0NBQUE7RUFDQSw2RkFBQTtBQUNKOztBQUVBO0VBQ0ksZ0NBQUE7QUFDSjs7QUFFQTtFQUNJLGdDQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQ0FBQTtFQUNBLG1DQUFBO0VBQ0EsZ0JBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7QUFDSjs7QUFFQTtFQUNJLGFBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7QUFDSjs7QUFFQTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7RUFFQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0FBQUo7O0FBR0E7RUFDSSxpQkFBQTtFQUVBLGFBQUE7RUFFQSxjQUFBO0VBQ0EsbUJBQUE7QUFGSjs7QUFLQTtFQUNJLGFBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FBRkoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9mb3RvLXBlcmZpbC9mb3RvLXBlcmZpbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XHJcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XHJcbiAgICAtLWJhY2tncm91bmQ6IHVybChcIi9zcmMvYXNzZXRzL2ltZy9iZy11YWktbGV2YTIucG5nXCIpICNlYWVhZWEgbm8tcmVwZWF0IGJvdHRvbSA3NXB4IGxlZnQgNDVweDtcclxufVxyXG5cclxuaW9uLXRvb2xiYXIge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjMDAxYzRlICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbmlvbi1oZWFkZXIge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjMDAxYzRlICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbmlvbi1pdGVtIHtcclxuICAgIC0taW9uLWl0ZW0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICAtLWJvcmRlci1zdHlsZTogdmFyKC0tYm9yZGVyLXN0eWxlKTtcclxuICAgIC0tYm9yZGVyOiAwIG5vbmU7IFxyXG4gICAgLS1ib3gtc2hhZG93OiAwIDAgMCAwOyBcclxuICAgIC0tb3V0bGluZTogMDtcclxufVxyXG5cclxuaW9uLWljb24ge1xyXG4gICAgY29sb3I6IGdyYXk7XHJcbn1cclxuXHJcbmltZyB7XHJcbiAgICBoZWlnaHQ6IDIwMHB4O1xyXG4gICAgd2lkdGg6IDIwMHB4O1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBtYXJnaW46IGF1dG87XHJcbn1cclxuXHJcbmgxIHtcclxuICAgIGZvbnQtc2l6ZTogMS41cmVtO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgLy90ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgbWFyZ2luOiAxNXB4IDA7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGNvbG9yOiAjMTIzYjdkO1xyXG59XHJcblxyXG4uZGVjbGFyYWNhbyB7XHJcbiAgICBmb250LXNpemU6IDEuMXJlbTtcclxuICAgIC8vZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgLy9wYWRkaW5nLWJvdHRvbTogMHB4O1xyXG4gICAgY29sb3I6ICMxMjNiN2Q7XHJcbiAgICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xyXG59XHJcblxyXG4udGVybW8ge1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIHBhZGRpbmctdG9wOiAwcHg7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICBtYXJnaW4tdG9wOiAtMjBweDtcclxuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XHJcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/pages/foto-perfil/foto-perfil.page.ts":
    /*!*******************************************************!*\
      !*** ./src/app/pages/foto-perfil/foto-perfil.page.ts ***!
      \*******************************************************/

    /*! exports provided: FotoPerfilPage */

    /***/
    function srcAppPagesFotoPerfilFotoPerfilPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FotoPerfilPage", function () {
        return FotoPerfilPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var FotoPerfilPage = /*#__PURE__*/function () {
        function FotoPerfilPage() {
          _classCallCheck(this, FotoPerfilPage);
        }

        _createClass(FotoPerfilPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return FotoPerfilPage;
      }();

      FotoPerfilPage.ctorParameters = function () {
        return [];
      };

      FotoPerfilPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-foto-perfil',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./foto-perfil.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/foto-perfil/foto-perfil.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./foto-perfil.page.scss */
        "./src/app/pages/foto-perfil/foto-perfil.page.scss"))["default"]]
      })], FotoPerfilPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-foto-perfil-foto-perfil-module-es5.js.map