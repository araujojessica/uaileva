(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-cnh-cnh-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/cnh/cnh.page.html":
    /*!*******************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/cnh/cnh.page.html ***!
      \*******************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesCnhCnhPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button text=\"\" color=\"light\"></ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <h1>Tire uma foto do(a) Carteira Nacional de Habilitação com EAR - CNH</h1>\n  <br>\n  <div>\n    <p class=\"termo\">\n      Lembre-se, é preciso que o documento: 1. Seja como na imagem abaixo. 2. Esteja aberto.\n      3. Tenha todos os campos visíveis. 4. Tenha a observação \"Exerce Atividade Remunerada\"\n      (EAR). Caso não tenha, consulte o Detran local e resolva no mesmo dia. Obs: A Permissão\n      para Dirigir não é válida\n    </p>\n  </div>\n\n  <img src=\"assets/img/exemplo-cnh.png\"><br>\n\n  <ion-button class=\"botao\">Enviar foto</ion-button>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/cnh/cnh-routing.module.ts":
    /*!*************************************************!*\
      !*** ./src/app/pages/cnh/cnh-routing.module.ts ***!
      \*************************************************/

    /*! exports provided: CNHPageRoutingModule */

    /***/
    function srcAppPagesCnhCnhRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CNHPageRoutingModule", function () {
        return CNHPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _cnh_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./cnh.page */
      "./src/app/pages/cnh/cnh.page.ts");

      var routes = [{
        path: '',
        component: _cnh_page__WEBPACK_IMPORTED_MODULE_3__["CNHPage"]
      }];

      var CNHPageRoutingModule = function CNHPageRoutingModule() {
        _classCallCheck(this, CNHPageRoutingModule);
      };

      CNHPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], CNHPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/cnh/cnh.module.ts":
    /*!*****************************************!*\
      !*** ./src/app/pages/cnh/cnh.module.ts ***!
      \*****************************************/

    /*! exports provided: CNHPageModule */

    /***/
    function srcAppPagesCnhCnhModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CNHPageModule", function () {
        return CNHPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _cnh_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./cnh-routing.module */
      "./src/app/pages/cnh/cnh-routing.module.ts");
      /* harmony import */


      var _cnh_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./cnh.page */
      "./src/app/pages/cnh/cnh.page.ts");

      var CNHPageModule = function CNHPageModule() {
        _classCallCheck(this, CNHPageModule);
      };

      CNHPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _cnh_routing_module__WEBPACK_IMPORTED_MODULE_5__["CNHPageRoutingModule"]],
        declarations: [_cnh_page__WEBPACK_IMPORTED_MODULE_6__["CNHPage"]]
      })], CNHPageModule);
      /***/
    },

    /***/
    "./src/app/pages/cnh/cnh.page.scss":
    /*!*****************************************!*\
      !*** ./src/app/pages/cnh/cnh.page.scss ***!
      \*****************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesCnhCnhPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content {\n  --background: var(--ion-color-light);\n  --background: url(\"/src/assets/img/bg-uai-leva2.png\") #eaeaea no-repeat bottom 75px left 45px;\n}\n\nion-toolbar {\n  --background: #001c4e !important;\n}\n\nion-header {\n  --background: #001c4e !important;\n}\n\nion-item {\n  --ion-item-background: transparent;\n  --border-style: var(--border-style);\n  --border: 0 none;\n  --box-shadow: 0 0 0 0;\n  --outline: 0;\n}\n\nion-icon {\n  color: gray;\n}\n\nimg {\n  height: 300px;\n  width: 300px;\n  display: block;\n  margin: auto;\n}\n\nh1 {\n  font-size: 1.5rem;\n  text-align: center;\n  margin: 15px 0;\n  font-weight: bold;\n  color: #123b7d;\n}\n\n.declaracao {\n  font-size: 1.1rem;\n  padding: 10px;\n  color: #123b7d;\n  text-align: justify;\n}\n\n.termo {\n  padding: 10px;\n  padding-top: 0px;\n  color: #123b7d;\n  margin-top: -20px;\n  text-align: justify;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY25oL2NuaC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxvQ0FBQTtFQUNBLDZGQUFBO0FBQ0o7O0FBRUE7RUFDSSxnQ0FBQTtBQUNKOztBQUVBO0VBQ0ksZ0NBQUE7QUFDSjs7QUFFQTtFQUNJLGtDQUFBO0VBQ0EsbUNBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0EsWUFBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtBQUNKOztBQUVBO0VBQ0ksYUFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtBQUNKOztBQUVBO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtFQUVBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7QUFBSjs7QUFHQTtFQUNJLGlCQUFBO0VBRUEsYUFBQTtFQUVBLGNBQUE7RUFDQSxtQkFBQTtBQUZKOztBQUtBO0VBQ0ksYUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUFGSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NuaC9jbmgucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xyXG4gICAgLS1iYWNrZ3JvdW5kOiB1cmwoXCIvc3JjL2Fzc2V0cy9pbWcvYmctdWFpLWxldmEyLnBuZ1wiKSAjZWFlYWVhIG5vLXJlcGVhdCBib3R0b20gNzVweCBsZWZ0IDQ1cHg7XHJcbn1cclxuXHJcbmlvbi10b29sYmFyIHtcclxuICAgIC0tYmFja2dyb3VuZDogIzAwMWM0ZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5pb24taGVhZGVyIHtcclxuICAgIC0tYmFja2dyb3VuZDogIzAwMWM0ZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5pb24taXRlbSB7XHJcbiAgICAtLWlvbi1pdGVtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgLS1ib3JkZXItc3R5bGU6IHZhcigtLWJvcmRlci1zdHlsZSk7XHJcbiAgICAtLWJvcmRlcjogMCBub25lOyBcclxuICAgIC0tYm94LXNoYWRvdzogMCAwIDAgMDsgXHJcbiAgICAtLW91dGxpbmU6IDA7XHJcbn1cclxuXHJcbmlvbi1pY29uIHtcclxuICAgIGNvbG9yOiBncmF5O1xyXG59XHJcblxyXG5pbWcge1xyXG4gICAgaGVpZ2h0OiAzMDBweDtcclxuICAgIHdpZHRoOiAzMDBweDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG59XHJcblxyXG5oMSB7XHJcbiAgICBmb250LXNpemU6IDEuNXJlbTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIC8vdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgIG1hcmdpbjogMTVweCAwO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBjb2xvcjogIzEyM2I3ZDtcclxufVxyXG5cclxuLmRlY2xhcmFjYW8ge1xyXG4gICAgZm9udC1zaXplOiAxLjFyZW07XHJcbiAgICAvL2ZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIC8vcGFkZGluZy1ib3R0b206IDBweDtcclxuICAgIGNvbG9yOiAjMTIzYjdkO1xyXG4gICAgdGV4dC1hbGlnbjoganVzdGlmeTtcclxufVxyXG5cclxuLnRlcm1vIHtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMHB4O1xyXG4gICAgY29sb3I6ICMxMjNiN2Q7XHJcbiAgICBtYXJnaW4tdG9wOiAtMjBweDtcclxuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XHJcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/pages/cnh/cnh.page.ts":
    /*!***************************************!*\
      !*** ./src/app/pages/cnh/cnh.page.ts ***!
      \***************************************/

    /*! exports provided: CNHPage */

    /***/
    function srcAppPagesCnhCnhPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CNHPage", function () {
        return CNHPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var CNHPage = /*#__PURE__*/function () {
        function CNHPage() {
          _classCallCheck(this, CNHPage);
        }

        _createClass(CNHPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return CNHPage;
      }();

      CNHPage.ctorParameters = function () {
        return [];
      };

      CNHPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-cnh',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./cnh.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/cnh/cnh.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./cnh.page.scss */
        "./src/app/pages/cnh/cnh.page.scss"))["default"]]
      })], CNHPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-cnh-cnh-module-es5.js.map