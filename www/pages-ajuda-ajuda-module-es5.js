(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-ajuda-ajuda-module"], {
    /***/
    "./src/app/pages/ajuda/ajuda-routing.module.ts":
    /*!*****************************************************!*\
      !*** ./src/app/pages/ajuda/ajuda-routing.module.ts ***!
      \*****************************************************/

    /*! exports provided: AjudaPageRoutingModule */

    /***/
    function srcAppPagesAjudaAjudaRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AjudaPageRoutingModule", function () {
        return AjudaPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ajuda_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./ajuda.page */
      "./src/app/pages/ajuda/ajuda.page.ts");

      var routes = [{
        path: '',
        component: _ajuda_page__WEBPACK_IMPORTED_MODULE_3__["AjudaPage"]
      }];

      var AjudaPageRoutingModule = function AjudaPageRoutingModule() {
        _classCallCheck(this, AjudaPageRoutingModule);
      };

      AjudaPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], AjudaPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/ajuda/ajuda.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/pages/ajuda/ajuda.module.ts ***!
      \*********************************************/

    /*! exports provided: AjudaPageModule */

    /***/
    function srcAppPagesAjudaAjudaModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AjudaPageModule", function () {
        return AjudaPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _ajuda_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./ajuda-routing.module */
      "./src/app/pages/ajuda/ajuda-routing.module.ts");
      /* harmony import */


      var _ajuda_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./ajuda.page */
      "./src/app/pages/ajuda/ajuda.page.ts");

      var AjudaPageModule = function AjudaPageModule() {
        _classCallCheck(this, AjudaPageModule);
      };

      AjudaPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _ajuda_routing_module__WEBPACK_IMPORTED_MODULE_5__["AjudaPageRoutingModule"]],
        declarations: [_ajuda_page__WEBPACK_IMPORTED_MODULE_6__["AjudaPage"]],
        exports: [_ajuda_page__WEBPACK_IMPORTED_MODULE_6__["AjudaPage"]]
      })], AjudaPageModule);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-ajuda-ajuda-module-es5.js.map