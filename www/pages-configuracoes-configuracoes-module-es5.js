(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-configuracoes-configuracoes-module"], {
    /***/
    "./src/app/pages/configuracoes/configuracoes-routing.module.ts":
    /*!*********************************************************************!*\
      !*** ./src/app/pages/configuracoes/configuracoes-routing.module.ts ***!
      \*********************************************************************/

    /*! exports provided: ConfiguracoesPageRoutingModule */

    /***/
    function srcAppPagesConfiguracoesConfiguracoesRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ConfiguracoesPageRoutingModule", function () {
        return ConfiguracoesPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _configuracoes_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./configuracoes.page */
      "./src/app/pages/configuracoes/configuracoes.page.ts");

      var routes = [{
        path: '',
        component: _configuracoes_page__WEBPACK_IMPORTED_MODULE_3__["ConfiguracoesPage"]
      }];

      var ConfiguracoesPageRoutingModule = function ConfiguracoesPageRoutingModule() {
        _classCallCheck(this, ConfiguracoesPageRoutingModule);
      };

      ConfiguracoesPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ConfiguracoesPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/configuracoes/configuracoes.module.ts":
    /*!*************************************************************!*\
      !*** ./src/app/pages/configuracoes/configuracoes.module.ts ***!
      \*************************************************************/

    /*! exports provided: ConfiguracoesPageModule */

    /***/
    function srcAppPagesConfiguracoesConfiguracoesModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ConfiguracoesPageModule", function () {
        return ConfiguracoesPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _configuracoes_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./configuracoes-routing.module */
      "./src/app/pages/configuracoes/configuracoes-routing.module.ts");
      /* harmony import */


      var _configuracoes_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./configuracoes.page */
      "./src/app/pages/configuracoes/configuracoes.page.ts");

      var ConfiguracoesPageModule = function ConfiguracoesPageModule() {
        _classCallCheck(this, ConfiguracoesPageModule);
      };

      ConfiguracoesPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _configuracoes_routing_module__WEBPACK_IMPORTED_MODULE_5__["ConfiguracoesPageRoutingModule"]],
        declarations: [_configuracoes_page__WEBPACK_IMPORTED_MODULE_6__["ConfiguracoesPage"]],
        exports: [_configuracoes_page__WEBPACK_IMPORTED_MODULE_6__["ConfiguracoesPage"]]
      })], ConfiguracoesPageModule);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-configuracoes-configuracoes-module-es5.js.map