import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AcordosPage } from './acordos.page';

describe('AcordosPage', () => {
  let component: AcordosPage;
  let fixture: ComponentFixture<AcordosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcordosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AcordosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
