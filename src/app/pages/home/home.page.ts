import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { LoadingController, Platform } from '@ionic/angular';
import {Environment, Geocoder, GoogleMap, GoogleMapOptions, GoogleMaps, GoogleMapsAnimation, GoogleMapsEvent, ILatLng, Marker, MyLocation} from '@ionic-native/google-maps'
import {
  NavController,
  AlertController,
  MenuController,
  ToastController,
  PopoverController,  
  ModalController, } from '@ionic/angular';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { PagamentosPage } from '../pagamentos/pagamentos.page';

declare var google: any;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  @ViewChild('map',{static:true}) mapElement: any;
  loading: any;
  map: GoogleMap;
  search: string = '';
  searchOrigem: string = '';
  googleAutocomplete = new google.maps.places.AutocompleteService();
  searchResults = new Array<any>();
  searchResultsOrigem = new Array<any>();
  originMarker: Marker;
  originMarkerDiferente: Marker;
  destination: any;
  origem: any;
  googleDirectionsService = new google.maps.DirectionsService();
  matrix = new google.maps.DistanceMatrixService();
  valor = new Array<any>();
  endOrigin: any;
  corrida: any;
  tempoEstimadoCorrida: any;
  valorCorrida: any;
 // user$: Observable<User>;
  valores:Object[] = [];

  constructor(
    private platform: Platform, 
    private loadCtrl: LoadingController,
    private ngZone: NgZone,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
  //  private userService: UserService,
    public loadingController : LoadingController,
    private router: Router,
    //private viagemService:ViagemService
    
  ) {

    //this.user$ = userService.getUser();
 

   
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }

  ngOnInit(): void {

    this.mapElement = this.mapElement.nativeElement;
    this.mapElement.style.width = this.platform.width() + 'px';
    this.mapElement.style.height = this.platform.height() + 'px';

    this.loadMap();
    
  }

  async loadMap(){
    this.loading = await this.loadCtrl.create({message: 'Por favor aguarde ...'});
    await this.loading.present();
    
    Environment.setEnv({
      'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyBBSA6wXmuyhUqg1fDMH4-WP3QZSRSDtr4',
      'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyBBSA6wXmuyhUqg1fDMH4-WP3QZSRSDtr4'
    });

    const mapOptions: GoogleMapOptions = {
      controls: {
        zoom: false
      }
    }
    this.map = GoogleMaps.create(this.mapElement, mapOptions);

    try {
      await this.map.one(GoogleMapsEvent.MAP_READY);
      this.addOrigenMarker();
      
    } catch (error) {
      console.error(error)
    }
      
  }
 
  async addOrigenMarker() {

    try {
      const myLocation: MyLocation = await this.map.getMyLocation();
      await this.map.moveCamera({
        target: myLocation.latLng,
        zoom: 18
      }) ;
     
     this.originMarker=  this.map.addMarkerSync({
        title: 'Origem',
        icon: '#000',
        animation: GoogleMapsAnimation.DROP,
        position: myLocation.latLng
      });
    } catch (error) {
      console.error(error)
    }finally{
      this.loading.dismiss();
    }
    
  }

  searchChangedOrigem(){
    if (!this.searchOrigem.trim().length) return;
  
    this.googleAutocomplete.getPlacePredictions({input: this.searchOrigem}, predictions => {
      this.ngZone.run(() =>{
        this.searchResultsOrigem = predictions;
       
      });
    });
  }
  searchChanged(){
    if (!this.search.trim().length) return;

    this.googleAutocomplete.getPlacePredictions({input: this.search}, predictions => {
      this.ngZone.run(() =>{
        this.searchResults = predictions;
      });
    
      
    });
  }
  async calcRouteOrigem(item: any){
    this.origem = item;
    const info: any = await Geocoder.geocode({address: this.origem.description})
    
    this.originMarkerDiferente = this.map.addMarkerSync({
      title: this.origem.description,
      icon: '#000',
      animation: GoogleMapsAnimation.DROP,
      position: info[0].position
    });
   
  }
  async calcRoute(item: any){
    this.search = '';
     
   this.destination = item;
     const info: any = await Geocoder.geocode({address: this.destination.description})

   let markerDestination: Marker = this.map.addMarkerSync({
     title: this.destination.description,
     icon: '#000',
     animation: GoogleMapsAnimation.DROP,
     position: info[0].position
   });

   
   console.log('distancia origem: ' + this.originMarker.getPosition().lat + " long :  " + this.originMarker.getPosition().lng)
   console.log('distancia destino: ' + markerDestination.getPosition());
   

    if(this.originMarkerDiferente == undefined){
        this.originMarkerDiferente = this.originMarker;
    }

   this.googleDirectionsService.route({
     origin: this.originMarkerDiferente.getPosition(),
     destination: markerDestination.getPosition(),
     travelMode: 'DRIVING'
   }, async results =>{
      const points = new Array<ILatLng>();
      const routes = results.routes[0].overview_path;
     
      
      for(let i = 0; i < routes.length; i++){
        points[i] = {
          lat: routes[i].lat(),
          lng: routes[i].lng()
        }
        
      }
      await this.map.addPolyline({
        points: points,
        color: 'red',
        width: 3
   });
   var origin1 = new google.maps.LatLng(this.originMarkerDiferente.getPosition().lat, this.originMarkerDiferente.getPosition().lng);
   var destinationB = new google.maps.LatLng(markerDestination.getPosition().lat, markerDestination.getPosition().lng);
   
   console.log(origin1);
   console.log(destinationB)
   this.matrix.getDistanceMatrix(
    {
      origins: [origin1],
      destinations: [destinationB],
      travelMode: google.maps.TravelMode.DRIVING
    }, callback);
  
  function callback(response, status) {
    let travelDetailsObject;
    if (status !== "OK") {
      alert("Error was: " + status);
    } else {
      var origins = response.originAddresses;
       var destinations = response.destinationAddresses;
       console.log(origins)
       console.log(destinations)
       for (var i = 0; i < origins.length; i++) {
         var results = response.rows[i].elements;
         for (var j = 0; j < results.length; j++) {
            var element = results[j];
            var distance = element.distance.text;
            var duration = element.duration.text;
            var from = origins[i];
            var to = destinations[j];
            travelDetailsObject = {
               distance: distance,
               duration: duration
            }
         }
       }
       this.travelDetailsObject = travelDetailsObject;
       
       this.valor = this.travelDetailsObject;

       this.corrida = 'distancia : ' + this.travelDetailsObject.distance + ' no tempo de : ' + this.travelDetailsObject.duration;
       console.log('distancia : ' + this.valor.distance + ' no tempo de : ' + this.travelDetailsObject.duration)
       
    }
    
  }
  
      this.map.moveCamera({target:points});
      this.map.panBy(0,100);
   });
 
  
  }
 
 

  async back(){

    try {
      await this.map.clear();
      this.destination = null;
      this.origem = null;
      this.searchOrigem = null;
      document.getElementById("collapseExample").style.display == 'none'
      
      this.addOrigenMarker();
    } catch (error) {
      console.error(error)
    }
  }


  toggleTeste() {
    if (document.getElementById("collapseExample").style.display == 'none') {
        document.getElementById("collapseExample").style.display = "block";

    } else {
        document.getElementById("collapseExample").style.display = "none";
    }
  }

  toggleTeste1() {
    if (document.getElementById("collapseExample1").style.display == 'none') {
        document.getElementById("collapseExample1").style.display = "block";

    } else {
        document.getElementById("collapseExample1").style.display = "none";
    }
  }

  toggleCarregar() {
    if (document.getElementById("carregando").style.display == 'none') {
        document.getElementById("carregando").style.display = "block";

    } else {
        document.getElementById("carregando").style.display = "none";
    }
  }

  async carregandoAlerta() {
   // console.log(this.userService.getUser())
    /*let newViagem: Viagem;
    newViagem.destino_final = "brazil";
    newViagem.destino_inicio = "brazil";
    newViagem.user_id = "1";
    newViagem.quilometragem_estimada = "10km";
    newViagem.status = "AGUARDANDO";
    newViagem.valor_estimado= "20.00";*/

    /*this.viagemService.signup(newViagem).subscribe(
        
      async () => {

        const toast = await (await this.toastCtrl.create({
          message: 'Teste cadastrado com sucesso!',
          duration: 4000, position: 'top',
          color: 'success'
        })).present();
        this.router.navigate(['/teste2'])
      },  
      async err => {
        console.log(err)
        const toast = await (await this.toastCtrl.create({
          message: 'Houve um erro, por favor avise o administrador do app!',
          duration: 4000, position: 'top',
          color: 'danger'
        })).present();
      }
    );*/


    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: 'carregar',
      subHeader: 'Aguardando motorista',
      message: `<p></p><ion-spinner name="bubbles"></ion-spinner>`,
      buttons: ['Cancelar corrida']
    });

    await alert.present();
  }

  async embarqueRapido() {
    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: '',
      subHeader: '',
      message: 'Embarque rápido : <br> • Pegue um UaiLeva <br> • Escaneie o QR Code <br> • Inicie a corrida <br> Registrar o Cartão',
      buttons: ['X']
    });

    await alert.present();
  }

  logout(){
   // this.userService.logout();
    this.router.navigate(['']);
  }

  async showModalPagamentos(){
    const modal = await this.modalCtrl.create({
      component: PagamentosPage
    });

    modal.present();
  }

}
