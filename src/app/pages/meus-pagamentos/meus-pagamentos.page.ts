import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-meus-pagamentos',
  templateUrl: './meus-pagamentos.page.html',
  styleUrls: ['./meus-pagamentos.page.scss'],
})
export class MeusPagamentosPage implements OnInit {

  constructor(
    public modalCtrl: ModalController,
  ) { }

  ngOnInit() {
  }

  close(){
    this.modalCtrl.dismiss();
  }

}
