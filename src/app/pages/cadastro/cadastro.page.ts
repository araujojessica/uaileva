import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {

  tipo: boolean;

  constructor(
    public router: Router,
  ) { }

  ngOnInit() {
  }

  mostrarSenha(){
    this.tipo = !this.tipo;
  }

  toEtapas(){
    this.router.navigate(['/etapas']);
  }
}
