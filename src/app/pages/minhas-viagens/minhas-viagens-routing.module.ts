import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MinhasViagensPage } from './minhas-viagens.page';

const routes: Routes = [
  {
    path: '',
    component: MinhasViagensPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MinhasViagensPageRoutingModule {}
