import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CNHPage } from './cnh.page';

const routes: Routes = [
  {
    path: '',
    component: CNHPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CNHPageRoutingModule {}
