import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CNHPage } from './cnh.page';

describe('CNHPage', () => {
  let component: CNHPage;
  let fixture: ComponentFixture<CNHPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CNHPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CNHPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
