import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  tipo: boolean;

  constructor(
    public router: Router,
  ) { }

  ngOnInit() {
  }

  mostrarSenha(){
    this.tipo = !this.tipo;
  }

  goToHome(){
    this.router.navigate(['/home']);
  }

  cadastro(){
    this.router.navigate(['cadastro']);
  }

}
