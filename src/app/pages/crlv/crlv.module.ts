import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CRLVPageRoutingModule } from './crlv-routing.module';

import { CRLVPage } from './crlv.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CRLVPageRoutingModule
  ],
  declarations: [CRLVPage]
})
export class CRLVPageModule {}
