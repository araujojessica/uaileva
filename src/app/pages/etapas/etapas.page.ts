import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-etapas',
  templateUrl: './etapas.page.html',
  styleUrls: ['./etapas.page.scss'],
})
export class EtapasPage implements OnInit {

  constructor(
    public router: Router
  ) { }

  ngOnInit() {
  }

  toAcordos(){
    this.router.navigate(['/acordos']);
  }

  toFotoPerfil(){
    this.router.navigate(['/foto-perfil']);
  }

  toCNH(){
    this.router.navigate(['/cnh']);
  }

  toCRLV(){
    this.router.navigate(['/crlv']);
  }

}
