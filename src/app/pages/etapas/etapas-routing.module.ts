import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EtapasPage } from './etapas.page';

const routes: Routes = [
  {
    path: '',
    component: EtapasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EtapasPageRoutingModule {}
