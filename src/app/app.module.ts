import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import {GoogleMaps} from '@ionic-native/google-maps';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { MensagensPage } from './pages/mensagens/mensagens.page';
import { MinhasViagensPage } from './pages/minhas-viagens/minhas-viagens.page';
import { MeusPagamentosPage } from './pages/meus-pagamentos/meus-pagamentos.page';
import { AjudaPage } from './pages/ajuda/ajuda.page';
import { ConfiguracoesPage } from './pages/configuracoes/configuracoes.page';
import { AvaliacoesPage } from './pages/avaliacoes/avaliacoes.page';

@NgModule({
  declarations: [
    AppComponent,
    MensagensPage,
    MinhasViagensPage,
    MeusPagamentosPage,
    AjudaPage,
    ConfiguracoesPage,
    AvaliacoesPage
  ],
  entryComponents: [
    MensagensPage,
    MinhasViagensPage,
    MeusPagamentosPage,
    AjudaPage,
    ConfiguracoesPage,
    AvaliacoesPage
  ],
  imports: [
    BrowserModule,
   
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
   
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: LocationStrategy,useClass: HashLocationStrategy},
 
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
