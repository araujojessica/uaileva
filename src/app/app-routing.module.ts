import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule) },
  { path: 'login', loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule) },
  { path: 'pagamentos', loadChildren: () => import('./pages/pagamentos/pagamentos.module').then( m => m.PagamentosPageModule) },
  { path: 'mensagens', loadChildren: () => import('./pages/mensagens/mensagens.module').then( m => m.MensagensPageModule) },
  { path: 'meus-pagamentos', loadChildren: () => import('./pages/meus-pagamentos/meus-pagamentos.module').then( m => m.MeusPagamentosPageModule) },
  { path: 'minhas-viagens', loadChildren: () => import('./pages/minhas-viagens/minhas-viagens.module').then( m => m.MinhasViagensPageModule) },
  { path: 'ajuda', loadChildren: () => import('./pages/ajuda/ajuda.module').then( m => m.AjudaPageModule) },
  { path: 'configuracoes', loadChildren: () => import('./pages/configuracoes/configuracoes.module').then( m => m.ConfiguracoesPageModule) },
  { path: 'avaliacoes', loadChildren: () => import('./pages/avaliacoes/avaliacoes.module').then( m => m.AvaliacoesPageModule) },
  { path: 'cadastro', loadChildren: () => import('./pages/cadastro/cadastro.module').then( m => m.CadastroPageModule) },
  { path: 'etapas', loadChildren: () => import('./pages/etapas/etapas.module').then( m => m.EtapasPageModule) },
  { path: 'acordos', loadChildren: () => import('./pages/acordos/acordos.module').then( m => m.AcordosPageModule) },
  { path: 'foto-perfil', loadChildren: () => import('./pages/foto-perfil/foto-perfil.module').then( m => m.FotoPerfilPageModule) },
  { path: 'cnh', loadChildren: () => import('./pages/cnh/cnh.module').then( m => m.CNHPageModule) },
  { path: 'crlv', loadChildren: () => import('./pages/crlv/crlv.module').then( m => m.CRLVPageModule) },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
