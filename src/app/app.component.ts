import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController, ModalController, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { MensagensPage } from './pages/mensagens/mensagens.page';
import { MinhasViagensPage } from './pages/minhas-viagens/minhas-viagens.page';
import { MeusPagamentosPage } from './pages/meus-pagamentos/meus-pagamentos.page';
import { AjudaPage } from './pages/ajuda/ajuda.page';
import { ConfiguracoesPage } from './pages/configuracoes/configuracoes.page';
import { AvaliacoesPage } from './pages/avaliacoes/avaliacoes.page';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public menuCtrl: MenuController,
    public router: Router,
    public modalCtrl: ModalController,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  async showModalMensagens(){
    const modal = await this.modalCtrl.create({
      component: MensagensPage
    });

    return await modal.present();
  }

  async toMinhasViagens(){
    const modal = await this.modalCtrl.create({
      component: MinhasViagensPage
    });

    return await modal.present();
  }

  async toMeusPagamentos(){
    const modal = await this.modalCtrl.create({
      component: MeusPagamentosPage
    });

    return await modal.present();
  }

  async toAjuda(){
    const modal = await this.modalCtrl.create({
      component: AjudaPage
    });

    return await modal.present();
  }

  async toConfiguracoes(){
    const modal = await this.modalCtrl.create({
      component: ConfiguracoesPage
    });

    return await modal.present();
  }

  async toAvaliacoes(){
    const modal = await this.modalCtrl.create({
      component: AvaliacoesPage
    });

    return await modal.present();
  }

  /*funcaoRota(){
    this.router.navigate(['/funcaoRota']);
    this.menuCtrl.toggle();
  }*/
}
